# PICRUSt2 docker image

This builds a docker images [psbcbb/picrust2](https://hub.docker.com/r/psbcbb/picrust2) from a [conda environment](picrust2.environment.yml) for [PICRUSt2 v 2.5.0](https://github.com/picrust/picrust2).

- [debian](debian): based on [continuumio/miniconda3:4.12.0](https://hub.docker.com/layers/miniconda3/continuumio/miniconda3/4.12.0/images/sha256-58b1c7df8d69655ffec017ede784a075e3c2e9feff0fc50ef65300fc75aa45ae?context=explore); compressed sif file ~ 800MB; version 4.12 of miniconda3 (newest image available at time of this writing)
  - docker image: [psbcbb/picrust2:2.5.0](https://hub.docker.com/layers/picrust2/psbcbb/picrust2/2.5.0/images/sha256-bc1598214adbb18311dfc7e7eac7f72f1c1cee5f2545991ebd0f9142f58b0cd2?context=explore)
- [alpine](alpine): based on [continuumio/miniconda3:4.10.3p0-alpine](https://hub.docker.com/layers/miniconda3/continuumio/miniconda3/4.10.3p0-alpine/images/sha256-dc976a37da75088ab3d458ab266910b0b5afa2c9d971b01ae0cdcd4cc3a97247?context=explore); compressed sif file ~ 700MB; (older) version 4.10 of miniconda3; smaller size, and all picrust2 programs work, but may be missing some convenient unix programs
  - docker image: [psbcbb/picrust2:2.5.0_alpine](https://hub.docker.com/layers/picrust2/psbcbb/picrust2/2.5.0_alpine/images/sha256-68dead116c109613ab98af1824475869a76f17f09f1329e224fb7f082c466a46?context=explore)


To build from either folder (debian or alpine) with your own tagname (e.g. 2.5.0 or 2.5.0_alpine):

```bash
cd path/to/picrust2_image
docker build -t username/picrust2:tagname -f debian/Dockerfile .
docker push username/picrust2:tagname
```

**Note:** Anaconda will sometimes delete old packages, so this build may not work in the future (hence the creation of a docker image).

#### Build notes

This uses a [multi-stage build](https://pythonspeed.com/articles/conda-docker-image-size/) to reduce the size (uncompressed 1.75 gb vs 3.84 gb for single stage).

This is built primarily for use with singularity on a remote cluster where the user will not be root (and the username may not be known).  So, it's a little tricky to automatically activate the picrust2 conda environment.  To do this, the Dockerfile:

1. Adds [custom.bashrc](custom.bashrc) which activates the environment.

2. From [here](https://stackoverflow.com/questions/57416579/activate-conda-environment-in-singularity-container-from-dockerfile), sets `BASH_ENV=/opt/etc/custom.bashrc` .  This will activate the environment for `singularity run`.
3. From [here](https://stackoverflow.com/a/64911137), adds [entrypoint.sh](entrypoint.sh) script to activate the environment for `singularity exec`.

