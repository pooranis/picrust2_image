#!/bin/bash

set -e

# source $BASH_ENV set to custom.bashrc to activate picrust2 env and let the following process take over
source $BASH_ENV
exec "$@"
